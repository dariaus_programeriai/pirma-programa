﻿#Papildytas failas

#dar karta papildytas is narsykles
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Configuration;
using System.Threading;

namespace scales
{
    public class svarst
    {
        static SerialPort scalesport = new SerialPort(settings.parametrai.svarstykliu_portas);
        public static string xxx = "", scale_msg ="";
        public static double tikras = 0;
        static string atsakymas, klaida, svoris;
        public static void DIBAL()
        {
            byte[] c = new byte[] { 0x39, 0x38, 0x30, 0x30, 0x30, 0x30, 0x31, 0x39 ^ 0x38 ^ 0x30 ^ 0x30 ^ 0x30 ^ 0x30 ^ 0x31, 0x0D, 0x0A };
            scalesport.WriteTimeout = 500;
            scalesport.ReadTimeout = 500;

            if (scalesport.IsOpen == true)
                {
                    scalesport.Close();
                }
            try
            {
                scalesport.Open();
                scalesport.Write(c, 0, c.Length);
                atsakymas = scalesport.ReadLine();
                Console.WriteLine(atsakymas);
                klaida = atsakymas.Substring(2, 1);
                svoris = atsakymas.Substring(3, 5);
                tikras = double.Parse(svoris) / 1000;
                
                if (tikras == 0)
                {
                    scale_msg = "Uždėkite svorį!";
                    tikras = 1;
                    return;
                }
                switch (klaida)
                {
                    case "1":
                        scale_msg = "Nestabilus svoris!\nBandykite dar kartą!";
                        tikras = 1;
                        break;
                    case "0":
                        scale_msg = "";
                        break;
                }

                scalesport.Close();
                
            }
            catch (Exception ex)
            {
                if (ex.Message == "The operation has timed out.")
                {
                    tikras = 1;
                    scale_msg = "Svarstyklės neatsako.\nPatikrinkite COM portą!";
                }
                else if (ex.Message == "Input string was not in a correct format.")
                {
                    scale_msg = "Minusinis svoris!\nPatikrinkite svarstykles!";
                    tikras = 1;
                }
                else Console.WriteLine(ex.Message);
            }
        }
        public static void AND()
        {
            byte[] c = new byte[] { 0x05, 0x12 };

            scalesport.WriteTimeout = 500;
            scalesport.ReadTimeout = 500;
            if (scalesport.IsOpen == true)
            {
                scalesport.Close();
            }
            try
            {
                scalesport.Open();
                scalesport.Write(c, 0, c.Length);
                Thread.Sleep(100);
                atsakymas = scalesport.ReadExisting();
                switch (atsakymas.Length < 14)
                {
                    case true:
                        scale_msg = "Nėra ryšio su\nsvarstyklėmis!";
                        break;
                }
                klaida = atsakymas.Substring(14, 1);
                svoris = atsakymas.Substring(15, 7);
                tikras = double.Parse(svoris) / 1000;
                
                if (tikras == 0)
                {
                    scale_msg = "Uždėkite svorį!";
                    tikras = 1;
                    return;
                }
                if (tikras < 0)
                {
                    scale_msg = "Minusinis svoris!\nPatikrinkite svarstykles!";
                    tikras = 1;
                    return;
                }
                switch (klaida)
                {
                    case "U":
                        scale_msg = "Nestabilus svoris!\nBandykite dar kartą!";
                        break;
                    case "S":
                        scale_msg = "";
                        break;
                }
                scalesport.Close();
            }                
            catch (Exception ex)
            {
                if (ex.Message == "The operation has timed out.")
                {
                    tikras = 0;
                    scale_msg = "Svarstyklės neatsako.\nPatikrinkite COM portą!";
                }
                else Console.WriteLine(ex.Message);
            }
        }
    }
}
